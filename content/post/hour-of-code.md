---
title: Hour of Code for Teachers
subtitle: Something for Every STEM Teacher
featured: "img/hour-of-code-logo.png"
date: 2018-09-06
tags: ["web design"]
type: post
---

![Hour of Code Logo](/img/hour-of-code-logo.png "Hour of Code Logo")

Have you heard about the [Hour of Code](https://hourofcode.com/us)? It's a wonderful initiative put on by [Code.org](https://www.code.org). Their goal is go get every student to program at least one hour of code each year. That might not sound like much, but think about how many students never write any code. It's not necessarily true at every student needs to write code at a high level, but by exposing it to them at a young age, those future rock star engineers will get the bug and start self educating.

I think one of the most important things about the Hour of Code is for math and science teachers to step up to the table to host it at their schools. Yes, it would be great if every school had a computer science teacher to offer something like this, but the fact of the matter is, most don't, especially at the K-8 level. The good news is that the people at Hour of Code have developed activities that work with kids of all ages and don't require significant training or resources to run. Even if you can't get a computer lab or tablets, they have created pencil and paper coding activities.

The program officially runs during the national week of code, which is December 3-9, 2018. However, you can host it at your school any time. If you are a parent, reach out to your school to ask how they are going to be involved, and if they need help, volunteer your services. This is a great way to get involved and give back.
