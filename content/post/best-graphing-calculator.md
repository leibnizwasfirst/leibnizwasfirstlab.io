---
title: Best Graphing Calculators
subtitle: Do We Need Graphing Calculators in Year 2018?
date: 2018-07-15
tags: ["ti-84", "math", "calculator"]
type: post
---

![TI-Nspire CX](https://d33wubrfki0l68.cloudfront.net/410e7b8e506fd5654f5924f81dc1068678fa97ce/051a5/img/ti-nspire-cx-cas-small.jpg "TI-Nspire CX")

Back to school shopping is upon us, and I can't help but notice the displays at Walmart, Target, and the like. They're full of pencils, glue, binders, notebooks, and... graphing calculators? Aren't graphing calculator supposed to be a relic from the 1990's. How can we still be using these in the year 2018? Well, I've been looking into this question, and I think I have the answer.

While there are some nifty math apps out there for your standard smartphone, there are a few reason they haven't taken off. One, a lot of schools are still hesitant to let kids use phones in class, let alone require them. And I get that, it's a distraction. I had plenty of distractions as a student, so I can only imagine what that would have done to me at age 15. The other big one, and it's huge, is that the big standardizes tests don't allow smartphones either. The reasons there are pretty obvious. It opens up a wide world of cheating, airplane mode be damned.

So if you're a student, and you've got to get one, what is the [best graphing calculator](https://www.techpoweredmath.com/best-graphing-calculators-for-school/) in 2018? I see a couple of options that would be appealing to me. The first is the [TI-84 Plus CE](https://www.techpoweredmath.com/ti-84-plus-ce-review/). That' effectively the same graphing calculator I used in high school, the TI-82, with some nice upgrades, like a color screen, rechargeable battery, picture graphing, etc. If it was good enough for me...

The other option out there that really impresses me is the [TI-Nspire CX](https://www.techpoweredmath.com/ti-nspire-cx-review/). Texas Instruments took their whole graphing calculator platform and rebuilt it from the ground up. Given their level of success, that's bold. Again, all the modern convenience of the TI-84 Plus CE, but a much nicer interface. It has a nicer menu system, smoother graphing system, etc. It screams 2010's, man!

Just make sure you do your homework, because they don't give those babies away. A graphing calculator still costs over $100, which means that they have pretty much held their value for 20 years.

Further reading:

* [Official Texas Instruments site](https://education.ti.com/en)
* [TI-Nspire CX](https://en.wikipedia.org/wiki/TI-Nspire_series) (Wikipedia)
* [TI-84 Plus CE](https://en.wikipedia.org/wiki/TI-84_Plus_series) (Wikipedia)
