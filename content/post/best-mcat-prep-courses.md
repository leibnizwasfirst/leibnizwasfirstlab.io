---
title: Best MCAT Prep Courses
subtitle: More Competitive Than Ever
date: 2020-03-16
tags: ["test prep"]
type: post
---

I've been looking to do another test prep post, this time on the MCAT. The MCAT is the Medical College Admission Test. If you are a pre-med major, this test is in your future. Good grades alone won't be enough to get into any med school, let along a good med school. The process is extremely competitive. Just look at the plot below that I found that shows the percentiles for the MCAT.

<a href="https://www.techpoweredmath.com/img/mcat_percentile_plot.jpg"><img src="https://www.techpoweredmath.com/img/mcat_percentile_plot.jpg" alt="MCAT Scores"></a>

We can see from this plot that the median score on the MCAT is a 500 but only about 20% of students that score a 500 are admitted to med school. Being average isn't good enough. That's why a lot of students turn to MCAT prep services. If you're going to use a test prep service, what are your options? There are quite a few to consider, but here are some of the most popular.

* [Princeton Review](https://www.princetonreview.com/medical/mcat-test-prep?ceid=newhp-nav): Well reviewed by students. They are in most metros and offer online courses.
* [Kaplan](https://www.kaptest.com/mcat): Definitely the most pricey option, but quality courses. They offer "intensives" where you can go study on site with MCAT experts for a full month.
* [Khan Academy](https://www.khanacademy.org/test-prep/mcat): Hey, it's free. It's on demand, so don't expect to have your hand held, but the material is there.
* [Magoosh](https://mcat.magoosh.com/): They're the new kids on the block. Like most of the paid options on the list, they offer a score guarantee. Unlike the others, they are very affordable.

This isn't something you want to rush into. Take your time and evaluate your options. You'll need a lot of time to ramp up for the MCAT. I'd consider reading at least one of the many reviews of the [best MCAT prep courses](https://www.techpoweredmath.com/best-mcat-prep-course/) you'll find online. Also, you may want to consider buying an MCAT prep book. I like the ones from Kaplan, [available on Amazon](https://www.amazon.com/Complete-7-Book-Subject-Review-2020-2021/dp/1506248861/).
