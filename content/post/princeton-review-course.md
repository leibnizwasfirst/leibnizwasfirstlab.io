---
title: Princeton Review ACT Prep
subtitle: Still Relevant in 2018?
featured: "img/princeton_review.png"
date: 2018-09-04
tags: ["politics"]
type: post
---

![Princeton Review logo](/img/princeton_review.png "Princeton Review logo")

Back in my day, we walked 5 miles each way to school. Back in my day, we didn't need fancy [graphing calculators](/post/best-graphing-calculator/). Back in my day, we crammed around the radiator for heat each morning at the start of algebra class on a freezing cold January morning in a classroom that had dropped to freezing temperatures overnight. And back in my day, when we prepped for the ACT, we went to a [Princeton Review](https://www.princetonreview.com/) class in order to get ready for the biggest test of our lives.

But times change (thankfully). And I'm left to ask the question, does it still make sense to work with a test prep company as we head toward the 2020's? On the one hand, the ACT and SAT are still extremely important. Don't believe the hype about "test optional" schools. Standardized tests are still very important for admission at most schools. While some students with impeccable credentials don't need to submit a test, most students will still need to show up with a good score in order to punch their ticket.

On the other hand, some companies are trying to make test prep a commodity. You see places like [Khan Academy](https://www.khanacademy.org/test-prep/sat) doing cheap test prep. They have a plan that's free. Others are low cost.

Princeton Review has pushed back on this. Some of their top people have been seen in YouTube videos talking about exactly what they have to offer that moves them beyond this class: decades of experience, a passion for helping kids achieve their college kids, and an online and classroom experience that is second to none. [Reviews of the Princeton Review course](https://www.techpoweredmath.com/princeton-review-act-sat-prep-reviews/) seem to agree that it does offer something extra.

Regardless, it doesn't seem like Princeton Review is going anywhere anytime soon. It's true that it's expensive. However, as long as there are parents from affluent families that are eager to give their kids an advantage, companies like Princeton Review are going to have a platform for their services. To some extent, it's a two-tier system, but that's the world we live it.
