---
title: Hugo Static Site Generator
subtitle: Building Sites Without a CMS
featured: "img/hugo-logo.png"
date: 2018-06-05
tags: ["web design"]
type: post
---

![Hugo Static Site Generator](/img/hugo-logo.png "Hugo Static Site Generator")

If you are used to building sites with a traditional content management systems (CMS) like Wordpress, in this post, I'm going to tell you why you consider Hugo, a static site generator. This site is built in Hugo, and it is a pure joy to work in.

## Site Speed

When you work with a static site generator, you don't have the overhead of the databases associated with a CMS. So that means that when a user visits your site, the pages have are already there as static pages and don't have to be generated from the database. This results in MUCH FASTER load times for the user. No more need for caching plugins, and more worries about your site going to the front page of Reddit and crashing. I've seen it happen.

## Security

When you work with a CMS, you have to worry about hacks. Wordpress, Drupal, Joomla and the like are notoriously susceptible to exploits. If you are not constantly on top of updating the installation of the CMS and its plugins, you may find that your site has been hacked and exposed to malware. With Hugo, there's basically nothing to hack since again... static pages.

## Backups

With Wordpress, you need to backup your database in case something goes wrong. There are many possible ways to make this more efficient at this point, but it's still a thing. With Hugo, you have the whole thing version controlled on GitHub or GitLab. If something goes wrong, roll it back. If you use a solution like Netlify, you can even have a developmental branch where you look at new content you are creating.

If I've convinced you that Hugo is worth looking into, Netlify is a great site for hosting static Hugo sites. While you can do it on AWS at minimal cost, Netlify has some extras, automatically integrates with GitHub or GitLab, and is free for personal sites. Here's a tutorial on using their flavor of Hugo known as [Victor Hugo](https://www.netlify.com/blog/2016/09/21/a-step-by-step-guide-victor-hugo-on-netlify/). There's no reason you have to use Victor Hugo, but again, it comes with a few extras like [PostCSS](http://postcss.org/) and [Babel](https://babeljs.io/) for CSS and JavaScript compiling/transpiling.

{{< youtube qtIqKaDlqXo >}}
