---
title: Princeton Review vs. Kaplan
subtitle: Battle of the Test Prep Giants
featured: "img/princeton_review.png"
date: 2018-09-09
tags: ["test prep"]
type: post
---

![Princeton Review logo](/img/princeton_review.png "Princeton Review logo")

I recently wrote a post about [Princeton Review](/post/princeton-review-course/), and how it seems to still be going strong for ACT and SAT prep. It's come to my attention, however, that some people are asking about the differences between [Princeton Review vs. Kaplan](https://techpoweredmath.com/kaplan-vs-princeton-review-mcat/). It's understandable. These two test prep giants have been around for quite some time.

On the one hand, you've got [Kaplan](https://www.kaptest.com). They are the big dog in this battle. They've been around longer, have more employees, and are a more generalized education company in addition to test prep.

On the other hand, you've got [Princeton Review](https://princetonreview.com). It's hard to call them an upstart at this point, having been around for over 30 years. Still, to some extent they are David to Kaplan's Goliath. They were started by a couple of recent college grads that thought they knew how to challenge that big dog and train up a generation of successful test takers.

While these two still battle on tests like the ACT and SAT, there's no question that the biggest question for those comparing the two is on higher profile tests like the MCAT. That test is make or break. While there is pretty much a college for everyone, there is not a med school for everyone.

So how do you decide between the two? To some extent, it is a matter of preferences. They each have their own personalized product line, and it really matters which of those products you care about. For example, Kaplan has a very exclusive, fancy retreat that pre-med students can go on for several weeks to immerse themselves in their studies with few distractions. Fancy, but expensive. Princeton Review offers more customized options for tutoring. For example, if you want to do group tutoring sessions to cut down on the expense, Kaplan doesn't have that option, but Princeton Review does.

Whichever test prep service you choose, whichever test you take, good luck. I do not miss being in high stakes test prep mode. There is a thrill of adrenaline to be sure, but the pressure is intense. Some things are best left to the young.
