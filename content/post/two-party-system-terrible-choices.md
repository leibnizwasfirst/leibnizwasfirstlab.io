---
title: Two Parties, Terrible Choices
subtitle: South Carolina Fifth Another Example of Partisanship
featured: "img/ralph_normal.png"
date: 2018-06-15
tags: ["politics"]
type: post
---

Among the inherent flaws with the two party system is the terrible choices that it delivers to voters in the general election. Candidates that no one would really prefer if they had not had to go through the primary system somehow emerge through that process and voters find themselves asking, "How did we get these guys?" So much has been written on this topic with regards to the 2016 Presidential election that I won't rehash that here. Instead, let me give you an example that is playing out right now, the South Carolina 5th Congressional District. On Tuesday, voters went to the polls to make their choices on both the Republican and Democratic sides, and here's what the voters are left with for the general election.

On the Republican side, incumbent Ralph Norman ran unopposed. Mr. Norman is a multimillionaire real estate developer who won this seat in a special election just a year ago after [Mick Mulvaney](https://www.npr.org/sections/money/2018/03/28/597761300/episode-832-mulvaney-vs-the-cfpb) vacated the seat to join the Trump administration as the Director of OMB. At this point, however, none of that is what [Ralph Norman](https://www.cnn.com/2018/04/06/politics/ralph-norman-loaded-gun-sc-congressman/index.html) is best known for. He's best known for pulling out a loaded gun in a restaurant with some mothers who had been victims of gun violence who had asked to speak with him about the topic at a public form. Stay classy, Mr. Norman.

![Ralph Norman gun coverage](/img/ralph_normal.png "Ralph Norman gun coverage")

So maybe the Democrats can do better, right? They've nominated [Archie Parnell](https://www.postandcourier.com/politics/top-south-carolina-candidate-refuses-to-quit-congressional-race-after/article_a6548188-5d27-11e8-a155-bf21652315cd.html). Parnell is a former tax attorney for the Justice Department. However, it was recently discovered that he beat his ex-wife, an allegation which he confirmed. This occurred 45 years ago, but it's something he concealed for all these years from all those around him, including the Democratic Party, which has pledged to NOT support him in the general election. Despite all that, he got over 60% of the vote, averting the need for a runoff.

![Archie Parnell domestic abuse NYT](/img/archie_parnell.png "Archie Parnell domestic abuse NYT")

Where does this leave South Carolina voters? In a similar place to where they were last summer, when these exact same two gentlemen ran against each other in a very closely contested election where Norman won over Parnell by only a few points in a district that would expected to be much more heavily Republican. The difference now is that the process had more time to reveal more about the character about these two men, and it doesn't seem to have mattered a bit to primary voters. The choice will be exactly the same. I'm not to here to say these two guys are rotten to the core, but over and over across America, voters are asking, "Why can't we do better"?
