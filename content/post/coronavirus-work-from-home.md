---
title: Coronavirus and Work From Home
subtitle: Our changing reality
date: 2020-03-15
tags: ["careers"]
type: post
---

So we're several days into one of the craziest scares that any of us have seen in our lifetimes. How are you doing out there? My family and I are pretty fortunate. We've got a good house for this kind of thing, including a fenced backyard. We realized a couple of weeks ahead of time that panic buying would probably set in, so we stocked up early. We didn't hoard, but we've got what we need for a few weeks. But there is obviously the social isolation. What about that?

One thing I'm finding that is I'm actually OK with the work from home thing. Actually, I may be more than OK with it. I think this may be learning some things about myself, like this is a better way for me to work. I've always known I had some social anxiety at work, but just a few days into this thing, I'm amazed how much lower my stress level is as well as my ability to focus on my tasks at hand. It makes me seriously consider if this is something I should be looking at long term.

That said, I'm learning quickly this is not for everyone. I can see as my blood pressure is lowering that for other people it is rising. If you find yourself in that situation, take care of yourself. Exercise, rest, watch a good show, and if you need it, reach out for counseling. There are plenty of options right now to get help, and everything is moving online including therapy.
